from PyQt5.QtCore import QModelIndex, Qt, QVariant, pyqtSignal, pyqtSlot
from PyQt5.QtGui import QBrush, QColor, QFont

from package.models.DataFrameModel import DataFrameModel


class ProtocoleModel(DataFrameModel):
    volumesChanged = pyqtSignal(int, float)
    concentrationsChanged = pyqtSignal(int, float, float)
    dataChanged = pyqtSignal()

    def __init__(self, protocole, parent=None, editable=True):
        super().__init__(parent, editable)
        self.protocole = protocole
        self.headers = self.df.columns.tolist()
        self.minSteps = 1

        self.dataChanged.connect(self.on_data_changed)
        self.layoutChanged.connect(self.on_data_changed)

    @property
    def df(self):
        return self.protocole._df

    def flags(self, index):
        flags = Qt.ItemIsEnabled
        if index.row() != 0 and self.editable and index.column() == 0:
            flags |= Qt.ItemIsEditable
        return flags

    def data(self, index, role=Qt.DisplayRole):

        if not index.isValid():
            return None

        row = self.df.index[index.row()]
        col = self.df.columns[index.column()]

        if role == Qt.DisplayRole:
            if index.column() == 7:
                return QVariant(str(self.df.loc[row, col]))
            return QVariant(float(self.df.loc[row, col]))
        elif role == Qt.EditRole:
            return QVariant(float(self.df.loc[row, col]))
        elif role == Qt.StatusTipRole:
            if index.row() == 0:
                if self.df.iloc[row, 7]:
                    msg = str(self.df.iloc[row, 7])
                else:
                    msg = "no RMN data file associated yet"
                return QVariant("Reference step : {}".format(msg))
            elif index.column() == 0 and index.row() != 0:
                return QVariant("Edit added volume")
            elif index.row() >= self.minSteps:
                return QVariant(
                    "Titration step : no RMN data file associated yet")
            else:  # Show file name
                return QVariant("File : {}".format(str(self.df.iloc[row, 7])))
        elif role == Qt.FontRole:
            if (index.column() == 0 or index.row() == 0):
                font = QFont()
                font.setBold(True)
                return QVariant(font)
        elif role == Qt.TextAlignmentRole:
            if index.column() == 0:
                return Qt.AlignCenter
        elif role == Qt.BackgroundRole:
            if index.column() == 0 and index.row() != 0:
                return QBrush(QColor(100, 200, 255, 30))
        elif role == Qt.ForegroundRole:
            if index.row() == 0:
                return QBrush(QColor(0, 0, 0, 100))
            elif index.row() >= self.minSteps:
                return QBrush(QColor(100, 165, 0))
            elif index.column() == 0:
                return QBrush(QColor("orange"))
        return None

    def headerData(self, section, orientation, role=Qt.DisplayRole):

        if role == Qt.StatusTipRole:
            if orientation == Qt.Horizontal:
                if section == 5:
                    return str("Concentration ratio : [titrant] / [analyte]")
        if role != Qt.DisplayRole:
            return QVariant()

        if orientation == Qt.Horizontal:
            try:
                return str(self.headers[section])
            except (IndexError, ):
                return QVariant()
        elif orientation == Qt.Vertical:
            try:
                return "Step {}".format(self.df.index.tolist()[section])
            except (IndexError, ):
                return QVariant()

    def setHeaderData(self, section, orientation, value, role=Qt.EditRole):
        if orientation == Qt.Horizontal:
            self.headers[section] = value
            self.headerDataChanged.emit(Qt.Horizontal, section, section)
            return True
        return False

    def setData(self, index, value, role=Qt.DisplayRole):
        """Set the value to the index position depending on
        Qt::ItemDataRole and data type of the column
        Args:
            index (QModelIndex): Index to define column and row.
            value (object): new value.
            role (Qt::ItemDataRole): Specifies what you want to do.
        Raises:
            TypeError: If the value could not be converted to a known datatype.
        Returns:
            True if value is changed. Calls layoutChanged after update.
            False if value is not different from original value.
        """
        if not index.isValid():
            return False

        if value != index.data(role):

            self.layoutAboutToBeChanged.emit()
            row = self.df.index[index.row()]
            col = self.df.columns[index.column()]
            self.protocole.update_volumes({row: value})
            self.protocole.update()
            self.layoutChanged.emit()
            return True

        return False

    def insertRows(self, row, count, parent=QModelIndex()):
        self.beginInsertRows(parent, self.rowCount(), self.rowCount())
        self.protocole.add_volume(self.protocole.volumes[-1] or 1)
        self.endInsertRows()
        return True

    def removeRow(self, row, parent=QModelIndex()):
        self.layoutAboutToBeChanged.emit()
        self.beginRemoveRows(parent, row, row)
        self.protocole.remove_step(row)
        self.endRemoveRows()
        self.layoutChanged.emit()
        return True

    def refresh(self):
        self.layoutAboutToBeChanged.emit()
        self.protocole.update()
        self.layoutChanged.emit()

    def add_step(self):
        self.insertRows(self.rowCount(), 1)
        self.dataChanged.emit()

    def remove_step(self, index):
        if (self.hasIndex(index.row(), index.column()) and
                index.row() != 0):
            self.removeRow(index.row())
        elif self.rowCount() > 1:
            self.removeRow(self.rowCount() - 1)
        else:
            return False
        self.dataChanged.emit()

    def set_file_list(self, fileList: list):
        steps = len(fileList)
        self.minSteps = max(1, steps)
        while self.rowCount() < self.minSteps:
            self.add_step()
        self.protocole.set_file_list(fileList)
        self.dataChanged.emit()

    @pyqtSlot()
    def on_data_changed(self):
        self.volumesChanged.emit(self.rowCount(), self.maxTotalVolume)
        self.concentrationsChanged.emit(self.rowCount(),
                                        self.maxConcentration,
                                        self.maxRatio)
        if self.rowCount() <= self.minSteps:
            self.removeDisabled.emit(True)
        else:
            self.removeDisabled.emit(False)

    @property
    def maxTotalVolume(self):
        return self.data(self.index(self.rowCount() - 1, 2)).value()

    @property
    def maxConcentration(self):
        return max(self.data(self.index(self.rowCount() - 1, 3)).value(),
                   self.data(self.index(0, 4)).value())

    @property
    def maxRatio(self):
        return self.data(self.index(self.rowCount() - 1, 5)).value()
