from math import floor

import numpy as np
import pandas as pd

# Qt Components
from PyQt5.QtCore import (QIdentityProxyModel, QModelIndex,
                          QSortFilterProxyModel, Qt, QVariant, pyqtSignal,
                          pyqtSlot)
from PyQt5.QtGui import QColor

# Shiftumi app components
# from package.classes.Worker import Worker
from package.models.DataFrameModel import DataFrameModel


class DataFrame3DModel(DataFrameModel):

    def __init__(self, shiftumi, parent=None, editable=False):
        super().__init__(parent, editable)
        self.shiftumi = shiftumi
        self.parseOpt = {
            "header": "infer",
            "columnMap": [0, 1, 2]
        }

    @property
    def df(self):
        return self.shiftumi.df

    def clear(self):
        self.layoutAboutToBeChanged.emit()
        self.shiftumi.init_df()
        self.layoutChanged.emit()

    def subRowCount(self):
        if self.df.empty:
            return 0
        return len(self.df.index.unique(level=1))

    def rowCount(self, parent=QModelIndex()):
        if self.df.empty:
            return 0
        return self.df.index.size

    def columnCount(self, parent=QModelIndex()):
        if self.df.empty:
            return 0
        return len(self.df.columns.values)

    def headerData(self, section, orientation, role=Qt.DisplayRole):
        if role != Qt.DisplayRole:
            return QVariant()

        if orientation == Qt.Horizontal:
            try:
                return str(self.df.columns.tolist()[section])
            except (IndexError, ):
                return QVariant()
        elif orientation == Qt.Vertical:
            try:
                return str(self.df.index.unique(level=0)[section])
            except (IndexError, ):
                return QVariant()

    def remove_subrow(self, subrow, reindex=[]):
        self.layoutAboutToBeChanged.emit()
        self.df.drop(subrow, level=1, inplace=True)
        if reindex:
            self.df.index = pd.MultiIndex.from_arrays([
                self.df.index.get_level_values(0),
                self.df.groupby(level=0).cumcount()],
                names=reindex)
        self.layoutChanged.emit()

    def setData(self, index, value, role=Qt.DisplayRole):
        return True

    def data(self, index, role=Qt.DisplayRole):
        """Returns data from model at given index
        """
        if index.isValid() and role in [Qt.DisplayRole, Qt.UserRole]:
            row = index.row()
            col = index.column()
            return QVariant(str(self.df.iloc[row, col]))
        return None


class RMNDataframeModel(QSortFilterProxyModel):

    def __init__(self, parent=None):
        super().__init__(parent)
        self.targetSubRow = 0

    @property
    def df(self):
        return self.sourceModel().df

    @property
    def step(self):
        return min(self.targetSubRow, self.sourceModel().subRowCount() - 1)

    def rowCount(self, parent=QModelIndex()):
        if self.df.empty:
            return 0
        return len(self.df.index.levels[0])

    def completeCount(self):
        return np.sum(self.df.xs(self.step, level="step").all(axis="columns"))

    def remove_subrow(self, row, reindex=[]):
        self.sourceModel().remove_subrow(row, reindex)

    def data(self, index, role=Qt.DisplayRole):
        """Returns data from model at given index
        """
        if not index.isValid():
            return None
        step = self.step

        if step < 0:
            return None
        row = self.df.index.levels[0][index.row()]
        col = self.df.columns[index.column()]
        if role in [Qt.DisplayRole, Qt.UserRole]:
            return QVariant(str(self.df.loc[(row, step), col]))
        elif role == Qt.BackgroundRole:
            if self.df.loc[(row, step), col] == 0:
                return QColor(255, 0, 0, 50)
        return None

    @pyqtSlot(int)
    def set_target_sub_row(self, row: int):
        self.layoutAboutToBeChanged.emit()
        self.targetSubRow = min(row, self.sourceModel().subRowCount() - 1)
        self.layoutChanged.emit()

    def reindex(self, names):
        self.df.index = pd.MultiIndex.from_arrays([
            self.df.index.get_level_values(0),
            self.df.groupby(level=0).cumcount()],
            names=names)

    def index(self, row, column, parent=QModelIndex):
        return self.createIndex(row, column, parent)

    def parent(self, index):
        return QModelIndex()

    def mapFromSource(self, source):
        if self.df.empty:
            return self.index(source.row(), source.column(), source.parent())
        return self.index(
            floor(source.row() / self.sourceModel().subRowCount()),
            source.column(),
            source.parent())

    def mapToSource(self, proxy):
        if self.sourceModel() and proxy.isValid():
            return self.sourceModel().index(
                (self.sourceModel().subRowCount() * proxy.row()) + self.step,
                proxy.column(),
                proxy.parent())
        else:
            return QModelIndex()

    def headerData(self, section, orientation, role=Qt.DisplayRole):
        if role != Qt.DisplayRole:
            return QVariant()

        if orientation == Qt.Horizontal:
            try:
                return str(self.df.columns[section])
            except (IndexError, ):
                return QVariant()
        elif orientation == Qt.Vertical:
            try:
                return str(self.df.index.unique(level=0)[section])
            except (IndexError, ):
                return QVariant()


class IntensityDataFrameModel(DataFrame3DModel):

    modelUpdated = pyqtSignal()

    def __init__(self, shiftumi, parent=None):
        super().__init__(shiftumi.computed, parent)
        self.shiftumi = shiftumi
        self.targetSubRow = 0

    def set_cutoff(self, cutoff: float):
        self.shiftumi.set_cutoff(cutoff)
        if not self.df.empty:
            self.dataChanged.emit(self.createIndex(0, 0),
                                  self.createIndex(self.rowCount() - 1,
                                                   self.columnCount() - 1))

    def rowCount(self, parent=QModelIndex()):
        if self.df.empty:
            return 0
        return len(self.df.index.levels[0])

    # Slots

    @pyqtSlot(int)
    def set_target_sub_row(self, row: int):
        self.targetSubRow = min(row, self.subRowCount() - 1)
        self.dataChanged.emit(self.createIndex(0, 0),
                              self.createIndex(self.rowCount() - 1,
                                               self.columnCount() - 1))

    @pyqtSlot()
    def update(self):
        reset = self.df.empty
        if reset:
            self.modelAboutToBeReset.emit()
        else:
            self.layoutAboutToBeChanged.emit()

        self.shiftumi.computeDeltas()

        if reset:
            self.modelReset.emit()
        else:
            self.layoutChanged.emit()
        self.modelUpdated.emit()

    def columnCount(self, parent=QModelIndex()):
        return 4

    def data(self, index, role=Qt.DisplayRole):
        """Returns data from model at given index
        """
        row = self.df.index.levels[0][index.row()]
        col = index.column()
        step = min(self.targetSubRow, self.subRowCount() - 1)

        if role in [Qt.DisplayRole, Qt.UserRole]:
            val = self.df.loc[(row, step), "intensity"]
            # Virtual column : above cutoff
            if col == 1:
                val = val if val >= self.cutoff else 0
            # Virtual column : below cutoff
            elif col == 2:
                val = val if val < self.cutoff else 0
            # Virtual column : incomplete residues
            elif col == 3:
                if index.row() in self.shiftumi.incomplete['rows']:
                    val = self.df["intensity"].max()
                else:
                    val = 0
            return QVariant(str(val))
        return None

    @property
    def cutoff(self):
        return self.shiftumi.cutoff

    @property
    def df(self):
        return self.shiftumi.computed

    @property
    def maxCutoff(self):
        if self.df.empty:
            return 0
        return self.df["intensity"].max()


class SelectedResiduesModel(QSortFilterProxyModel):

    selectionChanged = pyqtSignal()

    def __init__(self, parent):
        super().__init__(parent)

    def init_selection(self):
        self.shiftumi.init_selection()
        self.invalidateFilter()
        self.selectionChanged.emit()

    def data(self, index, role=Qt.DisplayRole):
        index = self.mapToSource(index)
        if not index.isValid():
            return None
        row = self.df.index.levels[0][index.row()]
        if role in [Qt.DisplayRole, Qt.UserRole]:
            return str(row)
        return None

    def filterAcceptsRow(self, sourceRow: int, source_parent: QModelIndex):
        if self.df.empty or sourceRow >= len(self.shiftumi.selected):
            return False
        return self.shiftumi.selected[sourceRow]

    def setSelection(self):
        selected = [self.df.iloc[
            row * self.sourceModel().subRowCount() + self.step,
            0] >= self.cutoff
            for row in range(self.sourceModel().rowCount())]
        self.shiftumi.set_selected(selected)
        self.invalidateFilter()
        self.selectionChanged.emit()

    def addSelection(self):
        selected = [self.df.iloc[
            row * self.sourceModel().subRowCount() + self.step,
            0] >= self.cutoff
            for row in range(self.sourceModel().rowCount())]
        selected = np.bitwise_or(selected, self.shiftumi.selected)
        self.shiftumi.set_selected(selected)
        self.invalidateFilter()
        self.selectionChanged.emit()

    @property
    def shiftumi(self):
        return self.sourceModel().shiftumi

    @property
    def step(self):
        return min(self.sourceModel().targetSubRow,
                   self.sourceModel().subRowCount() - 1)

    @property
    def cutoff(self):
        return self.shiftumi.cutoff

    @property
    def df(self):
        return self.sourceModel().df


class ShiftmapModel(QIdentityProxyModel):

    def __init__(self, parent):
        super().__init__(parent)
        self.filter = False
        self.scale = False

    def stepCount(self):
        return self.sourceModel().subRowCount()

    @property
    def df(self):
        return self.sourceModel().df

    @property
    def shiftumi(self):
        return self.sourceModel().shiftumi

    @pyqtSlot(bool)
    def toggle_filter(self, toggle: bool):
        self.filter = toggle

    @pyqtSlot(bool)
    def setScale(self, scale: bool):
        self.scale = scale

    def axisRange(self):
        if self.filter and self.scale:
            resIndex = self.df.index.unique(level=0)
            selected = resIndex[self.shiftumi.selected]
            xSeries = self.df.loc[selected, "Proton"]
            ySeries = self.df.loc[selected, "Nitrogen"]
        else:
            xSeries = self.df["Proton"]
            ySeries = self.df["Nitrogen"]

        incompletes = self.shiftumi.incomplete["index"]
        xSeries = xSeries.drop(incompletes, level=0)
        ySeries = ySeries.drop(incompletes, level=0)
        return {
            "X": (xSeries.min(),
                  xSeries.max()),
            "Y": (ySeries.min(),
                  ySeries.max())
        }
