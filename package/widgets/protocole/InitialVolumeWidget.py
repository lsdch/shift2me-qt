from PyQt5.QtCore import pyqtSignal, pyqtSlot
from PyQt5.QtWidgets import QDoubleSpinBox, QWidget
from PyQt5 import uic
from PyQt5.QtCore import pyqtSignal, pyqtSlot
from PyQt5.QtWidgets import QLineEdit, QSpinBox, QWidget


class InitialVolumeWidget(QGroupBox):

    updated = pyqtSignal()

    def __init__(self, parent=None):
        super().__init__(parent)
        uic.loadUi("designer/initialvolumewidget", self)
        self.analyteInput = self.ui.analyteInitialVolume
        self.totalInput = self.ui.totalInitialVolume 

        self.analyteInput.valueChanged.connect(self.totalInput.setMinimum)
        self.analyteInput.valueChanged.connect(self.update_analyte)
        self.totalInput.valueChanged.connect(self.update_total)

    def set_protocole(self, model):
        self.protocole = model
        self.fetch()

    def fetch(self):
        self.analyteInput.setValue(self.protocole.analyteStartVol)
        self.totalInput.setValue(self.protocole.startVol)

    @pyqtSlot("double")
    def update_analyte(self, volume):
        self.protocole.set_analyte_volume(volume)
        self.updated.emit()

    @pyqtSlot("double")
    def update_total(self, volume):
        self.protocole.set_initial_volume(volume)
        self.updated.emit()
