from PyQt5.QtCore import pyqtSignal, pyqtSlot
from PyQt5.QtWidgets import QFrame
from PyQt5 import uic

class StockSolutionWidget(QFrame):

    updated = pyqtSignal()

    def __init__(self, parent=None):
        super().__init__(parent)
        uic.loadUi("designer/stocksolution.ui", self)
        self.nameInput.textEdited.connect(self.update_name)
        self.concentration.valueChanged.connect(self.update_concentration)

    def set_model(self, solution):
        self.model = solution
        self.fetch()

    def fetch(self):
        self.concentration.setValue(self.model.concentration)
        self.nameInput.setText(self.model.name)

    @pyqtSlot("QString")
    def update_name(self, name):
        self.model.setName(name)
        self.updated.emit()

    @pyqtSlot("int")
    def update_concentration(self, concentration):
        self.model.setConcentration(concentration)
        self.updated.emit()

class AnalyteWidget(StockSolutionWidget):
    def __init__(self, parent):
        super().__init__(parent)
        self.nameLabel.setText("Analyte")
        self.nameInput.setPlaceholderText("Analyte")

class TitrantWidget(StockSolutionWidget):
    def __init__(self, parent):
        super().__init__(parent)
        self.nameLabel.setText("Titrant")
        self.nameInput.setPlaceholderText("Titrant")
