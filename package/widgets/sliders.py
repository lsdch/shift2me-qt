from PyQt5.QtWidgets import QGroupBox, QSlider, QDoubleSpinBox
from PyQt5 import uic
from PyQt5.QtCore import pyqtSlot, pyqtSignal


class CutoffSlider(QGroupBox):

    valueChanged = pyqtSignal("double")

    def __init__(self, parent):
        super().__init__(parent)
        uic.loadUi("designer/cutoffslider.ui", self)
        self.slider = self.cutoffSlider
        self.floatbox = self.cutoffSpinBox
        # Events
        self.slider.valueChanged.connect(self.emit_change)
        self.slider.valueChanged.connect(self.update_view)
        self.slider.sliderMoved.connect(self.update_view)
        self.floatbox.valueChanged.connect(self.update_view)

    @pyqtSlot("int")
    @pyqtSlot("double")
    def update_view(self, cutoff):
        sender = self.sender()
        if sender is self.slider:
            self.floatbox.blockSignals(True)
            cutoff = cutoff/100.
            self.floatbox.setValue(cutoff)
            self.floatbox.blockSignals(False)
        elif sender is self.floatbox:
            self.slider.blockSignals(True)
            self.slider.setValue(cutoff*100)
            self.emit_change(cutoff)
            self.slider.blockSignals(False)

    @pyqtSlot("int")
    @pyqtSlot("double")
    def emit_change(self, cutoff):
        if self.sender() is self.slider:
            cutoff = cutoff/100.
        self.valueChanged.emit(cutoff)

    def setMaximum(self, value):
        self.slider.setMaximum(int(value))


class StepSlider(QGroupBox):
    stepChanged = pyqtSignal(int)

    def __init__(self, parent):
        super().__init__(parent)
        uic.loadUi("designer/stepslider.ui", self)
        self.slider = self.slider
        self.label = self.label

        # Signal
        self.slider.valueChanged.connect(self.sliderChanged)

    @pyqtSlot(int)
    def sliderChanged(self, step):
        self.label.setNum(step)
        self.stepChanged.emit(step)

    @pyqtSlot(int)
    def setStep(self, step):
        self.slider.setValue(step)

    @property
    def step(self):
        return self.slider.value()

    def setMaximum(self, value):
        self.slider.setRange(1, max(1, value))
        self.slider.setEnabled(value > 1)
