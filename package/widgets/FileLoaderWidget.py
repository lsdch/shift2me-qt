from glob import glob
from os import path

import qtawesome as qta
from PyQt5 import uic
from PyQt5.QtCore import QFileInfo, Qt, pyqtSignal, pyqtSlot
from PyQt5.QtWidgets import QWidget


class FileLoaderWidget(QWidget):

    fileListChanged = pyqtSignal(int)
    referenceFileChanged = pyqtSignal(str)

    def __init__(self, parent=None):
        super().__init__(parent)

        # Setup UI
        uic.loadUi("designer/fileloader.ui", self)
        # Update icons
        self.sortAscBtn.setIcon(qta.icon('fa.caret-up'))
        self.sortDescBtn.setIcon(qta.icon('fa.caret-down'))
        self.removeBtn.setIcon(qta.icon('fa.trash'))

        # Set up logic
        self.shiftumi = self.window().shiftumi

        # Tracks added file paths to avoid loading duplicates
        self.filePaths = set()

        # Autosort file list alphanumerically
        self.autosort = Qt.AscendingOrder

        # Handle file browser events
        self.fileBrowser.addFiles.connect(self.add_files)
        self.fileBrowser.addDir.connect(self.add_directory)

        # Handle drag n drop events on file list
        self.fileList.filesDropped.connect(self.add_files)
        self.fileList.directoryDropped.connect(self.add_directory)
        # Handle file selection events
        self.fileList.itemSelectionChanged.connect(self.selectedFileChanged)

        # Connect file addition/removal from file list widget to model
        self.removeBtn.clicked.connect(self.remove_selected)
        self.sortAscBtn.toggled.connect(self.toggleSort)
        self.sortDescBtn.toggled.connect(self.toggleSort)

        # Handle file list change events
        self.fileListChanged.connect(self.toggleBtn)
        self.fileList.listReordered.connect(self.listReordered)

    def trim_files(self, files: list):
        """
        Filters path list with already known files to avoid duplicates

        :return: List of QFileInfo generated from path
        """
        paths = sorted(list(
            set(files).difference(self.filePaths)
        ))
        return [QFileInfo(path) for path in paths]

    @pyqtSlot(list)
    def add_files(self, pathList: list):
        """Adds files to list
        """
        if not len(pathList):
            return
        # Filter out known paths
        fileInfoList = self.trim_files(pathList)
        # Update fileloader progress
        self.fileBrowser.setMaxProgress(len(self.fileList) + len(fileInfoList))

        # Parsing options
        if len(self.fileList) == 0:
            referenceFilePath = fileInfoList[0].absoluteFilePath()
            self.referenceFileChanged.emit(referenceFilePath)

        # Load and validate files
        for fileInfo in fileInfoList:
            path = fileInfo.absoluteFilePath()
            # Validate file
            diagnostic = self.validate_file(path)
            # Add to file list
            item = self.fileList.add_file(fileInfo, diagnostic)
            assert item.data(Qt.UserRole)["path"] == path
            # Add to known paths
            self.filePaths.add(path)
            # Update progress bar
            self.fileBrowser.progressNext()

        # Autosort file list if toggled
        if self.autosort is not False:
            self.sortList(order=self.autosort)

        self.fileList.selectDefaultRow()

        self.fileListChanged.emit(len(self.fileList))

    @pyqtSlot(str)
    def add_directory(self, directory):
        """Adds files from selected directory
        """
        types = ("*.tsv", "*.csv", "*.list")
        pathList = []
        for fileExt in types:
            pathList.extend(glob(path.join(directory, fileExt)))
        pathList.sort()

        self.add_files(pathList)
        return pathList

    def validate_file(self, path):
        file_df = self.shiftumi.load_csv(path)
        return self.shiftumi.validate_df(file_df)

    def reload_files(self):
        for row in range(len(self.fileList)):
            item = self.fileList.item(row)
            data = item.data(Qt.UserRole)
            diagnostic = self.validate_file(data["path"])
            data.update(diagnostic)
            item.setData(Qt.UserRole, data)
        self.fileListChanged.emit(len(self.fileList))
        self.fileList.currentRowChanged.emit(self.fileList.currentRow())

    @pyqtSlot()
    def remove_selected(self):
        removed = self.fileList.remove_selected()
        self.filePaths.difference_update(removed)
        self.fileListChanged.emit(len(self.fileList))
        self.referenceFileChanged.emit(self.referenceFile)

    @pyqtSlot()
    def selectedFileChanged(self):
        toggleRemove = bool(self.fileList.selectedItems())
        self.removeBtn.setEnabled(toggleRemove)  # no selection

    @pyqtSlot()
    def listReordered(self):
        self.toggleSort(False)
        self.referenceFileChanged.emit(self.referenceFile)

    @pyqtSlot()
    @pyqtSlot(bool)
    def toggleSort(self, toggle=False):
        if not toggle:
            self.autosort = False
            self.sortAscBtn.setChecked(False)
            self.sortDescBtn.setChecked(False)
        else:
            if self.sender() is self.sortDescBtn:
                self.autosort = Qt.DescendingOrder
                self.sortAscBtn.blockSignals(True)
                self.sortAscBtn.setChecked(False)
                self.sortAscBtn.blockSignals(False)
            elif self.sender() is self.sortAscBtn:
                self.autosort = Qt.AscendingOrder
                self.sortDescBtn.blockSignals(True)
                self.sortDescBtn.setChecked(False)
                self.sortDescBtn.blockSignals(False)
            self.sortList(order=self.autosort)

    def sortList(self, order=Qt.AscendingOrder):
        self.fileList.sortItems(order)
        self.referenceFileChanged.emit(self.referenceFile)
        self.fileList.currentRowChanged.emit(self.fileList.currentRow())

    @pyqtSlot(int)
    @pyqtSlot(bool)
    def toggleBtn(self, toggle):
        """Enables widgets if file list is not empty
        """
        toggle = bool(toggle)
        self.sortAscBtn.setEnabled(toggle)
        self.sortDescBtn.setEnabled(toggle)

    @property
    def empty(self):
        return len(self.fileList) == 0

    @property
    def referenceFile(self):
        if not self.empty:
            return self.fileList.item(0).data(Qt.UserRole)["path"]
        else:
            return ""

    @property
    def errors(self):
        return sum([
            len(self.fileList.item(row).data(Qt.UserRole)["errors"])
            for row in range(len(self.fileList))
        ])
