from PyQt5.QtChart import (QBarCategoryAxis, QChart, QLineSeries,
                           QStackedBarSeries, QVBarModelMapper)
from PyQt5.QtCore import QMargins, QPointF, Qt, pyqtSlot
from PyQt5.QtGui import QColor, QFont

from package.classes.CategoryAxis import CategoryAxis


class IntensityBarChart(QChart):

    def __init__(self, model):
        super().__init__()

        self.init_series()
        self.init_axis()
        self.init_mappers(model)

        # Configure title font
        self.setTitle("Chemical shift intensities")
        font = self.titleFont()
        font.setBold(True)
        self.setTitleFont(font)

        # Configure animations
        self.setAnimationOptions(QChart.SeriesAnimations)
        self.setAnimationDuration(500)

        # Hide legend
        self.legend().setVisible(False)

        # Confgure layout
        self.layout().setContentsMargins(0, 0, 0, 0)
        self.setMargins(QMargins(0, 0, 15, 0))

    def init_axis(self):
        self.createDefaultAxes()
        self.lineAxis = CategoryAxis(self)
        self.setAxisX(self.lineAxis, self.lineSeries)
        self.lineAxis.setTitleText("Residue #")
        # self.axisX(self.lineSeries).setTickInterval(10)

        self.barAxis = QBarCategoryAxis()
        self.barAxis.append(["a"])
        self.barAxis.setLabelsAngle(90)
        self.barAxis.setLabelsFont(QFont("free", 8))
        self.barAxis.setVisible(False)
        self.setAxisX(self.barAxis, self.series)

    def init_series(self):
        self.series = QStackedBarSeries()
        self.series.setBarWidth(0.5)
        self.series.setUseOpenGL(True)

        # Cut-off line
        self.lineSeries = QLineSeries()
        pen = self.lineSeries.pen()
        pen.setWidth(1)
        pen.setColor(QColor(255, 0, 0))
        pen.setStyle(Qt.DashLine)
        pen.setDashPattern([4, 4])
        self.lineSeries.setPen(pen)

        self.addSeries(self.series)
        self.addSeries(self.lineSeries)

    def init_mappers(self, model):
        # Titrant volume mapper
        self.mapper = QVBarModelMapper(self)
        # Set model
        self.mapper.setModel(model)
        self.mapper.setFirstBarSetColumn(1)
        self.mapper.setLastBarSetColumn(3)
        self.mapper.setSeries(self.series)

    @pyqtSlot(int)
    def move_line(self, cutoff):
        cutoff = cutoff/100.
        self.lineSeries.replace([
            QPointF(0, cutoff),
            QPointF(len(self.mapper.model().df.index.levels[0]), cutoff)
        ])

    def set_colors(self):
        # Bars above cutoff
        self.series.barSets()[0].setColor(QColor("orange"))
        # Bars below cutoff
        self.series.barSets()[1].setColor(QColor("#209fdf"))
        # Bars of incomplete residues
        incompleteBarset = self.series.barSets()[2]
        brush = incompleteBarset.brush()
        brush.setStyle(Qt.Dense4Pattern)
        incompleteBarset.setBrush(brush)
        incompleteBarset.setColor(QColor(0, 0, 0, 100))

    def update_axes(self, xCategories, maxY):
        # Update bar categories (not visible but required to scale chart)
        self.barAxis.setCategories(xCategories)
        # Update X axis range and labels
        self.lineAxis.format_axis(0, len(xCategories), 10)
        # Update Y axis range
        self.axisY().setRange(0, maxY)
