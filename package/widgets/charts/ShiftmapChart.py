from PyQt5.QtChart import QChart, QScatterSeries, QValueAxis, QVXYModelMapper
from PyQt5.QtCore import QPointF, Qt, pyqtSlot
from PyQt5.QtGui import QColor

from package.models.DataFrame3DModel import ShiftmapModel


class ShiftmapChart(QChart):
    markerSize = 8
    highlight = {
        "color": None,
        "width": 8,
        "alpha": 60
    }
    default = {
        "color": QColor(100, 100, 100, 50),
        "width": 0,
        "alpha": 60
    }

    def __init__(self):
        super().__init__()

        self.model = None
        # Highlighted series
        self.highlighted = None

        self.legend().setVisible(False)
        self.init_axis()

    def init_axis(self):
        # X Axis
        proton = QValueAxis()
        proton.setTitleText("Proton")
        proton.setTickCount(10)
        proton.setMinorTickCount(4)
        self.setAxisX(proton)

        # Y Axis
        nitrogen = QValueAxis()
        nitrogen.setTitleText("Nitrogen")
        nitrogen.setTickCount(10)
        nitrogen.setMinorTickCount(4)
        self.addAxis(nitrogen, Qt.AlignLeft)

    def setModel(self, model: ShiftmapModel):
        self.model = model
        self.removeAllSeries()

    def createSeries(self, residue: str):
        # Create series
        series = QScatterSeries()
        series.setName(residue)
        series.setMarkerSize(self.markerSize)
        self.addSeries(series)
        series.attachAxis(self.axisX())
        series.attachAxis(self.axisY())
        # Configure series' brush
        brush = series.brush()
        color = brush.color()
        color.setAlpha(self.default["alpha"])
        brush.setColor(color)
        series.setBrush(brush)
        # Configure series' pen
        pen = series.pen()
        pen.setColor(self.default["color"])
        pen.setWidth(self.default["width"])
        series.setPen(pen)
        # Connect events
        series.released.connect(self.selectResidue)
        series.hovered.connect(self.highlightResidue)
        return series

    def createMapper(self, series, firstRow: int, rowCount: int):
        # Configure mapper
        mapper = QVXYModelMapper(self)
        mapper.setFirstRow(firstRow)
        mapper.setRowCount(rowCount)
        mapper.setXColumn(0)
        mapper.setYColumn(1)
        mapper.setSeries(series)
        mapper.setModel(self.model)
        return mapper

    def next_row(self):
        if self.mapperMap:
            return (self.mapperMap[next(reversed(self.mapperMap))].firstRow() +
                    self.model.stepCount())
        return 0

    @pyqtSlot("QPointF")
    def selectResidue(self, point: QPointF):
        self.selectSeries(self.sender())

    @pyqtSlot("QPointF", bool)
    def highlightResidue(self, point: QPointF, hover: bool):
        series = self.sender()
        pen = series.pen()
        if hover:
            pen.setWidth(self.highlight["width"])
            pen.setColor(self.highlight["color"] or series.color())
        elif series is not self.highlighted:
            pen.setWidth(self.default["width"])
            pen.setColor(self.default["color"])
        series.setPen(pen)

    def selectSeries(self, series):
        if self.highlighted is not None:
            pen = self.highlighted.pen()
            pen.setColor(self.default["color"])
            pen.setWidth(self.default["width"])
            self.highlighted.setPen(pen)
        pen = series.pen()
        pen.setWidth(self.highlight["width"])
        pen.setColor(self.highlight["color"] or series.color())
        series.setPen(pen)
        self.highlighted = series

    def update_range(self):
        if self.model.df.empty:
            return
        axisRange = self.model.axisRange()
        # X Axis
        self.axisX().setRange(*axisRange['X'])
        self.axisX().setTickCount(10)
        self.axisX().applyNiceNumbers()
        # Y Axis
        self.axisY().setRange(*axisRange['Y'])
        self.axisY().setTickCount(10)
        self.axisY().applyNiceNumbers()
