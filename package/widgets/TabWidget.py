from PyQt5.QtWidgets import QTabBar, QTabWidget


class TabBar(QTabBar):

    def __init__(self, parent):
        super().__init__(parent)

    def wheelEvent(self, event):
        pass


class TabWidget(QTabWidget):

    def __init__(self, parent):
        super().__init__(parent)
        tabBar = TabBar(self)
        self.setTabBar(tabBar)
