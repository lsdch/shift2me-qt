from PyQt5.QtCore import pyqtSignal, pyqtSlot
from PyQt5.QtWidgets import QComboBox, QWidget


class DynamicComboWidget(QComboBox):
    "A combo box that saves its previous index"

    def __init__(self, parent=None):
        super().__init__(parent)
        self.prev = -1

    def update_options(self, options):
        self.clear()
        for opt in options:
            self.addItem(opt)
        self.update_prev()

    def update_prev(self):
        self.prev = self.currentIndex()

    @pyqtSlot(int)
    def setCurrentIndex(self, index):
        super().setCurrentIndex(index)
        self.update_prev()

    def wheelEvent(self, event):
        pass


class LinkedComboWidget(QWidget):
    """Linked DynamicComboWidgets will switch current indexes when
    their values collide"""

    valueChanged = pyqtSignal(list)

    def __init__(self, parent):
        super().__init__(parent)
        self.ignore = None

    def connectEvents(self):
        self.combos = self.findChildren(DynamicComboWidget)
        for combo in self.combos:
            combo.currentIndexChanged.connect(self.comboChange)
        self.update_options(["Column 0",
                             "Column 1",
                             "Column 2"])
        self.valueChanged.connect(self.parent().internalChange)

    @pyqtSlot(int)
    def comboChange(self, index):
        "Listens to combo changes and switches values upon collision"

        sender = self.sender()
        # Ignore event if sender change was triggered by self
        if sender is self.ignore:
            self.ignore = None
            return False
        elif sender.prev > -1:
            for combo in self.combos:
                if combo.currentIndex() == index and combo != sender:
                    # Switch colliding combo value to sender previous
                    self.ignore = combo
                    combo.setCurrentIndex(sender.prev)
                    sender.update_prev()
        self.valueChanged.emit(self.value())

    def update_options(self, options):
        "Updates all combos with the same options list"

        for index, combo in enumerate(self.combos):
            combo.update_options(options)
            combo.setCurrentIndex(index)

    def value(self):
        return [combo.currentIndex() for combo in self.combos]

    def set_values(self, values):
        for combo, val in zip(self.combos, values):
            combo.blockSignals(True)
            combo.setCurrentIndex(val)
            combo.blockSignals(False)
