from PyQt5.QtWidgets import QListView
from PyQt5.QtCore import pyqtSlot, pyqtSignal


class ResidueListView(QListView):

    currentRowChanged = pyqtSignal(int)

    def __init__(self, parent):
        super().__init__(parent)

    @pyqtSlot(bool)
    def setLocked(self, lock: bool):
        self.model().setLocked(lock)

    def setModel(self, model):
        super().setModel(model)
        self.selectionModel().currentRowChanged.connect(self.currentChanged)

    @pyqtSlot("QModelIndex", "QModelIndex")
    def currentChanged(self, current, previous):
        self.currentRowChanged.emit(current.row())
