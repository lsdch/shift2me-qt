from PyQt5.QtCore import QModelIndex, Qt, pyqtSignal, pyqtSlot
from PyQt5.QtWidgets import QHeaderView, QTableView

from package.delegates.SpinBoxDelegate import SpinBoxDelegate


class ProtocoleTableView(QTableView):

    hoverInput = pyqtSignal(QModelIndex)

    def __init__(self, parent=None):
        super().__init__(parent)

        # In-table spin-boxes
        self.delegate = SpinBoxDelegate()
        self.setItemDelegateForColumn(0, self.delegate)
        self.setItemDelegateForRow(0, self.itemDelegate())
        self.hoverInput.connect(self.delegate.hover)
        self.indexBeforeReset = None

    def setModel(self, model):
        super().setModel(model)
        # Stretch columns
        for sectionIndex in range(model.columnCount()):
            self.horizontalHeader().setSectionResizeMode(
                sectionIndex, QHeaderView.Stretch)
        self.setColumnHidden(6, True)
        self.setColumnHidden(7, True)

        self.model().modelAboutToBeReset.connect(self.saveCurrentIndex)
        self.model().modelReset.connect(self.restoreCurrentIndex)
        
        # self.model().modelReset.connect(self.refresh)

    def saveCurrentIndex(self):
        self.indexBeforeReset = self.currentIndex()

    def restoreCurrentIndex(self):
        if self.model().hasIndex(self.indexBeforeReset.row(),
                                 self.indexBeforeReset.column()):
            self.setCurrentIndex(self.indexBeforeReset)
        self.indexBeforeReset = None

    def open_editors(self):
        for row in range(1, self.rowCount):
            index = self.model().index(row, 0)
            self.openPersistentEditor(index)

    def flags(self, index):
        if index.column() == 0:
            return Qt.ItemIsEditable | Qt.ItemIsEnabled
        else:
            super().flags(index)

    def mouseMoveEvent(self, event):
        index = self.indexAt(event.pos())
        if index.column() == 0 and index.row() > 0:
            self.setCursor(Qt.PointingHandCursor)
        else:
            self.setCursor(Qt.ArrowCursor)
        self.hoverInput.emit(index)
        return super().mouseMoveEvent(event)

    def moveCursor(self, action, modifiers):
        if action == self.MoveNext:
            action = self.MoveDown
        elif action == self.MovePrevious:
            action = self.MoveUp
        return super().moveCursor(action, modifiers)

    def edit_row(self):
        self.setCurrentIndex(self.model().index(self.currentIndex().row(), 0))
        self.edit(self.currentIndex())

    def keyPressEvent(self, event):
        if event.key() == Qt.Key_Return:
            self.edit_row()
        else:
            super().keyPressEvent(event)

    @property
    def rowCount(self):
        return self.model().rowCount()

    # @property
    # def volumes(self):
    #     for row in range(self.rowCount):
    #         yield self.model().data(self.protocoleModel.index(row, 0),
    #                                 Qt.DisplayRole)




