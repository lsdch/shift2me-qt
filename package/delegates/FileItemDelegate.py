import qtawesome as qta
from PyQt5.QtCore import Qt
from PyQt5.QtGui import (QBrush, QColor, QFont, QFontMetrics, QPainter,
                         QRadialGradient)
from PyQt5.QtWidgets import QApplication, QStyle, QStyledItemDelegate


class FileItemDelegate(QStyledItemDelegate):
    def __init__(self, parent):
        super().__init__(parent)

    def paint(self, painter, option, index):
        super().paint(painter, option, index)
        painter.save()
        painter.setRenderHint(QPainter.Antialiasing)
        self.paintRightIcon(painter, option, index)
        painter.restore()

    def paintRightIcon(self, painter, option, index):
        icon = self.rightIcon(index)
        if icon is not None:
            r = option.rect.adjusted(option.rect.width() - 25, 5, -5, -5)
            icon.paint(painter, r, alignment=Qt.AlignRight)

    def rightIcon(self, index):
        if index.model().data(index, Qt.UserRole)["errors"]:
            return qta.icon("fa.times-circle", color="red")
        elif index.model().data(index, Qt.UserRole)["warnings"]:
            return qta.icon("fa.warning", color="orange")
        else:
            return None


class SelectedFileItemDelegate(FileItemDelegate):

    def __init__(self, parent):
        super().__init__(parent)

    def updateEditorGeometry(self, editor, option, index):
        rect = option.rect.adjusted(
            option.decorationSize.width() + 9, 2, -30, -option.rect.height()/2)
        editor.setGeometry(rect)

    def paintFileIcon(self, painter, option, index):
        icon = index.model().data(index, Qt.DecorationRole)
        iconSize = option.decorationSize
        iconPixmap = icon.pixmap(iconSize)
        painter.drawPixmap(option.rect.x()+3,
                           option.rect.y()+2,
                           iconPixmap)

    def paintStepBadge(self, painter, option, index):
        painter.save()
        painter.setPen(Qt.white)
        boldFont = QFont(option.font)
        boldFont.setBold(True)
        painter.setFont(boldFont)
        stepRect = option.rect.adjusted(
            5, option.rect.height()/2+4,
            -option.rect.width() + option.decorationSize.width(), -4)

        gradient = QRadialGradient(
            stepRect.left()+stepRect.width()/2,
            stepRect.top()+stepRect.height()/2,
            stepRect.width()*0.6)
        gradient.setColorAt(0, QColor("#1C3140"))
        gradient.setColorAt(1, QColor("#2C1F17"))
        gradient.setSpread(QRadialGradient.ReflectSpread)

        brush = QBrush(gradient)
        painter.setBrush(brush)
        painter.setPen(QColor("#2C1F17"))  # )
        # stepRect = stepRect.adjusted(5, 4, -5, -4)
        painter.drawEllipse(stepRect)
        painter.setPen(QColor("limegreen"))
        painter.drawText(stepRect, Qt.AlignCenter, str(index.row()))
        painter.restore()

    def paintItemText(self, painter, option, index):
        painter.save()
        # Paint item text
        textRect = option.rect.adjusted(
            option.decorationSize.width()+9, 0, 0, -option.rect.height()/2)
        painter.setFont(option.font)
        painter.setPen(Qt.white)
        painter.drawText(textRect,
                         Qt.AlignLeft | Qt.AlignVCenter,
                         index.model().data(index, Qt.DisplayRole))
        painter.restore()

    def paintRightIcon(self, painter, option, index):
        icon = self.rightIcon(index)
        if icon is not None:
            iconRect = option.rect.adjusted(
                option.rect.width() - 25, 5, -5, -option.rect.height()/2-5)
            badgeRect = iconRect.adjusted(
                1, -1, -1, 1
            )
            painter.save()
            painter.setPen(Qt.white)
            painter.setBrush(Qt.white)
            painter.drawEllipse(badgeRect)
            painter.restore()
            icon.paint(painter, iconRect, alignment=Qt.AlignRight)

    def paintSubText(self, painter, option, index):
        painter.save()
        # Paint sub text in small font, with eliding
        subTextRect = option.rect.adjusted(
            option.decorationSize.width()+9, option.rect.height()/2, -5, 0)
        smallFont = QFont(option.font)
        smallFont.setPointSize(8)
        metrics = QFontMetrics(smallFont)
        path = metrics.elidedText(
            index.model().data(index, Qt.UserRole)["path"],
            Qt.ElideLeft,
            subTextRect.width())
        painter.setFont(smallFont)
        painter.setPen(Qt.white)
        painter.drawText(subTextRect,
                         Qt.AlignLeft | Qt.AlignVCenter,
                         path)
        painter.restore()

    def paint(self, painter, option, index):
        style = QApplication.style()
        if option.widget:
            style = option.widget.style()
        if option.state & QStyle.State_Selected:
            painter.setRenderHint(QPainter.Antialiasing)

            style.drawControl(QStyle.CE_ItemViewItem,
                              option, painter, option.widget)

            self.paintFileIcon(painter, option, index)
            self.paintStepBadge(painter, option, index)
            self.paintItemText(painter, option, index)
            self.paintRightIcon(painter, option, index)
            self.paintSubText(painter, option, index)

        else:
            super().paint(painter, option, index)

    def sizeHint(self, option, index):
        size = super().sizeHint(option, index)
        size.setHeight(size.height() * 2)
        return size
