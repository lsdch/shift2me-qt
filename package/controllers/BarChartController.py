"""A controller to handle UI interactions on chemical shifts barchart
"""

from threading import Thread

import numpy as np
import pandas as pd

# PyQt components
from PyQt5.QtChart import QChartView
from PyQt5.QtCore import Qt, QThreadPool, pyqtSlot

from PyQt5.QtGui import QPainter
from PyQt5.QtWidgets import (QDoubleSpinBox, QFrame, QSlider)
# Shiftumi app components
from package.classes.Worker import Worker
from package.models.DataFrame3DModel import (IntensityDataFrameModel,
                                             SelectedResiduesModel)
from package.views.ResidueListView import ResidueListView
from package.widgets.charts.IntensityBarChart import IntensityBarChart
from package.widgets.ResidueSelectionWidget import ResidueSelectionWidget
from package.widgets.sliders import CutoffSlider, StepSlider


class BarChartController(QFrame):

    def __init__(self, parent):
        super().__init__(parent)
        # self.threadPool = QThreadPool()
        # self.worker = Worker(self.set_cutoff_task)
        # self.worker.setAutoDelete(False)

    def init_model(self,
                   intensityModel: IntensityDataFrameModel,
                   selectionModel: SelectedResiduesModel):

        # Bar chart model
        self.model = intensityModel
        self.selectionModel = selectionModel
        # Find children
        self.chartview = self.findChild(QChartView)
        self.selectionWidget = self.findChild(ResidueSelectionWidget)
        self.cutoffWidget = self.findChild(CutoffSlider, "cutoffWidget")
        self.stepWidget = self.findChild(StepSlider, "stepWidget")
        # Slider events
        self.cutoffWidget.valueChanged.connect(self.set_cutoff)
        self.stepWidget.stepChanged.connect(self.set_step)

        # Setup chart
        self.init_chart()
        self.cutoffWidget.slider.sliderMoved.connect(self.chart.move_line)

        # Setup list
        self.selectionWidget.setModel(self.selectionModel)
        self.model.modelReset.connect(self.onModelReset)

    def init_chart(self):
        # Create chart
        self.chart = IntensityBarChart(self.model)
        # insert in UI
        self.chartview.setChart(self.chart)
        self.chartview.setRenderHint(QPainter.Antialiasing)

    @property
    def step(self):
        return self.model.targetSubRow

    @pyqtSlot(int)
    def set_step(self, step=0):
        rowCount = self.model.subRowCount()
        if step == 0:
            step = self.step
        if step < 1 or step >= rowCount:
            step = max(1, rowCount - 1)
        self.model.set_target_sub_row(step)
        self.stepWidget.setStep(step)

    @pyqtSlot("double")
    @pyqtSlot()
    def set_cutoff(self, cutoff=None):
        # self.worker.set_args(cutoff)
        # self.threadPool.start(self.worker)
        self.model.set_cutoff(cutoff)
        # Display cutoff line
        self.chart.move_line(cutoff*100)

    # def set_cutoff_task(self, cutoff):
    #     self.model.set_cutoff(cutoff)
    #     self.chart.move_line(cutoff*100)

    @pyqtSlot(bool, int)
    def bar_info(self, status, index):
        barset = self.sender()
        if not status:
            self.window().statusBar().clearMessage()
        else:
            value = barset.at(index)
            self.window().statusBar().showMessage(
                "#{index} : {label} ; {delta} = {value}".format(
                    index=index,
                    label=self.model.df.index.levels[0][index],
                    delta=u"\u03B4",
                    value=str(round(value, 3))
                ))

    @pyqtSlot()
    def onModelReset(self):
        # Set chart colors
        if self.model.subRowCount() > 1:
            self.chart.set_colors()
            for barset in self.chart.series.barSets():
                barset.hovered.connect(self.bar_info)

    @pyqtSlot()
    def update(self):
        if self.model.subRowCount() > 1:
            # Set step range
            self.stepWidget.setMaximum(self.model.subRowCount() - 1)

            # Set max cut off
            if self.model.maxCutoff > 0:
                self.cutoffWidget.setMaximum(self.model.maxCutoff*100)
                self.chart.update_axes(self.model.df.index.levels[0],
                                       self.model.maxCutoff)

            self.set_step()
            self.set_cutoff(self.model.cutoff)
