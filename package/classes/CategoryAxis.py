from math import ceil, floor

from PyQt5.QtChart import QCategoryAxis


class CategoryAxis(QCategoryAxis):

    def __init__(self, parent):
        super().__init__(parent)
        self.setLabelsPosition(self.AxisLabelsPositionOnValue)

    def format_axis(self, min_value, max_value, step):
        # Reset categories
        for s in self.categoriesLabels():
            self.remove(s)
        self.setStartValue(min_value)
        # Add categories
        self.append(str(min_value), min_value)
        for i in range(ceil(min_value / step), floor(max_value / step) + 1):
            v = i * step
            self.append('%g' % v, v)
        self.append(str(max_value), max_value)
        self.setRange(min_value, max_value)
