#-------------------------------------------------
#
# Project created by QtCreator 2018-02-19T14:59:50
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Shift2Me
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    filedialog.cpp

HEADERS  += mainwindow.h \
    filedialog.h

FORMS    += mainwindow.ui \
    setupdialog.ui \
    stock_dialog.ui \
    filedialog.ui \
    cutoffslider.ui \
    stepslider.ui \
    cutoff.ui \
    solutions.ui \
    initialvolumewidget.ui \
    stocksolution.ui \
    protocole.ui \
    protocole.ui

DISTFILES += \
    SetupDialog.py \
    TestForm.ui.qml \
    Test.qml
